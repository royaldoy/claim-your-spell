const axios = require("axios");
const os = require("os");
const { log } = require("console");
const fs = require("fs");
const chalk = require("chalk");
const prompt = require("prompt-sync")();

async function getCoday(url, headers) {
  try {
    const response = await axios.get(url, { headers: headers });
    return response.data;
  } catch (error) {
    return [];
  }
}

async function postCoday(url, headers, $post = {}) {
  try {
    const response = await axios.post(url, $post, { headers: headers });
    return response.data;
  } catch (error) {
    return { message: "Gagal!" };
    console.error(error);
  }
}

function tanggal(timestamp_ms) {
  if (timestamp_ms <= 1) {
    return "-";
  }
  const timestamp = Math.floor(timestamp_ms / 1000);
  const date = new Date(timestamp * 1000);
  return date.toLocaleString("en-GB", { timeZone: "Asia/Jakarta" });
}

async function claimAllTask(headers) {
  try {
    const getId = await getCoday("https://wapi.spell.club/quest/1", headers);
    if (!getId.is_claimed) {
      const ids = getId.steps
        .filter((step) => step.is_passed == null)
        .map((step) => step.id);
      for (const id of ids) {
        await postCoday(
          `https://wapi.spell.club/quest/step/${id}/complete`,
          headers
        );
        console.log(chalk.success(`Task:${id} - Complete`));
      }
    }
  } catch (error) {}
}

async function claimMana(headers) {
  try {
    const jsClaim = await postCoday(
      "https://wapi.spell.club/claim?batch_mode=true",
      headers
    );

    if (jsClaim.id) {
      console.log(chalk.green(`Claim Mana : Success: ${jsClaim.id}`));
    } else {
      console.log(chalk.red(`Claim Mana : Fail: ${jsClaim.message}`));
    }
  } catch (error) {}
}

async function getUserInfo(headers) {
  try {
    const jsUser = await getCoday("https://wapi.spell.club/user", headers);
    console.log(chalk.blue(`${tanggal(Date.now())}`));
    console.log(chalk.blue(`Address ${jsUser.address}`));
    console.log(
      chalk.blue(`Balance : ${(jsUser.balance / 1000000).toFixed(2)}`)
    );
    console.log(chalk.blue(`Referral ${jsUser.invited_users}`));
  } catch (error) {}
}

async function upgradeBoost() {
  try {
    const jsClaim = await postCoday(
      "https://wapi.spell.club/upgrade?batch_mode=true",
      headers,
      { upgrade_type: "booster" }
    );

    console.log(chalk.green(`Upgrade Booster : Berhasil ${jsClaim.data}`));
    return true;
  } catch (error) {
    console.log(chalk.red(`Gagal Upgrade Booster`));
    return false;
    console.error("Error fetching data:", error);
  }
}

async function claimTask(headers) {
  try {
    const jsClaim = await postCoday(
      "https://wapi.spell.club/quest/1/claim?batch_mode=true",
      headers
    );
    if (jsClaim.id) {
      console.log(chalk.green(`Claim Task Spell : Success  ${jsClaim.id}`));
    } else {
      console.log(chalk.red(`Claim Task Spell : Fail: ${jsClaim.message}`));
    }
  } catch (error) {}
}

async function doRunBot() {
  // const isWindows = os.platform() === "win32";
  const data = fs.readFileSync("data.txt", "utf-8").split("\r\n");
  process.stdout.write("\x1Bc");
  for (const dat of data) {
    const headers = {
      accept: "application/json, text/plain, */*",
      origin: "https://wallet.spell.club",
      referer: "https://wallet.spell.club/",
      Authorization: "tma " + dat,
    };

    console.log("===============================");
    const userInfo = await getUserInfo(headers);
    const manaGet = await claimMana(headers);
    const allTask = await claimAllTask(headers);
    const singleTask = await claimTask(headers);
    const booster = await upgradeBoost(headers);
    console.log("===============================");
  }
}

doRunBot();
